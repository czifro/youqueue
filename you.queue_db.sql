create table Network(
NetworkId int primary key IDENTITY(1,1) not null,
NetworkName nvarchar(20),
NetworkUrl nvarchar(max),
ThumbUrl nvarchar(max)
);

create table Series(
SeriesId int primary key IDENTITY(1,1) not null,
NetworkId int foreign key references Network(NetworkId),
SeriesName nvarchar(max),
SeriesUrl nvarchar(max),
Genre nvarchar(max),
Year nvarchar(15),
UpdateDay nvarchar(15),
Thumbnail nvarchar(max)
);

create table Episodes(
EpisodeId int primary key IDENTITY(1,1) not null,
SeriesId int foreign key references Series(SeriesId),
EpisodeName nvarchar(max),
Description nvarchar(max),
EpisodeNum int,
SeasonNum int,
WeeksLeft int,
Thumbnail nvarchar(max)
);

create table Accounts(
AccountId int primary key IDENTITY(1,1) not null,
Username nvarchar(20),
Password nvarchar(20),
Email nvarchar(20),
);

create table QueueItems(
QItemId int primary key IDENTITY(1,1) not null,
UserId int foreign key references Accounts(AccountId),
NetworkId int foreign key references Network(NetworkId),
SeriesId int foreign key references Series(SeriesId),
EpisodeId int foreign key references Episodes(EpisodeId),
DeleteItem int
);

create table Notifictions(
NotificationId int primary key IDENTITY(1,1) not null,
UserId int foreign key references Accounts(AccountId),
Message nvarchar(50),
Date datetime
);