﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace YouQueueService.NetworkServices
{
    public class WebPageDownloader
    {
        private HttpWebRequest _webRequest;
        private HttpWebResponse _webResponse;

        public string GetHtmlAsString(string url)
        {
            _webRequest = (HttpWebRequest)WebRequest.Create(url);
            _webResponse = (HttpWebResponse)_webRequest.GetResponse();
            return GetResponse();
        }

        public string XmlHttpRequest(string url)
        {
            _webRequest = (HttpWebRequest)WebRequest.Create(url);
            _webRequest.Accept = "application/xml, text/xml, */*; q=0.01";
            var referer = "http://www.tbs.com/video/index.jsp?cid=" +
                             url.Substring(url.IndexOf("id=") + "id=".Length);
            _webRequest.Referer = referer;

            _webResponse = (HttpWebResponse)_webRequest.GetResponse();
            return GetResponse();
        }


        private string GetResponse()
        {
            if (_webResponse == null)
                throw new NullReferenceException("webResponse");
            return new StreamReader(_webResponse.GetResponseStream(), Encoding.Default).ReadToEnd();
        }
    }
}
