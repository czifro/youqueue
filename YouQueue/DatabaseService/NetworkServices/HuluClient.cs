﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;

namespace YouQueueService.NetworkServices
{
    public static class HuluClient
    {
        private static HttpWebRequest _webRequest;
        private static HttpWebResponse _webResponse;
        private static StreamReader _reader;
        private static WebProxy _proxy;
        private static DateTime timeReceivedAccessToken;


        #region Hulu Url & Paths
        public const string HuluBaseUrl = "http://www.hulu.com";
        private const string HuluNetworksPath = "/mozart/v1.h2o/shows/companies?";
        private const string HuluSeriesPath = "/mozart/v1.h2o/companies/:companyId/all?";
        private const string HuluEpisodesPath1 = "/mozart/v1.h2o/videos/all?";
        private const string HuluEpisodesPath2 = "/mozart/v1.h2o/shows/:showId/episodes?";
        #endregion

        #region Editable Hulu Url Params
        public static string HuluParamMode = "whitelist_networks_only";
        public static string HuluParamSort = "";  // popular_this_month, networks, seasons_and_release, episodes, popular_today, series
        public static string HuluParamAccessToken = "";
        public static string HuluShowId = "";
        public static string HuluNetworkId = "";
        public static string HuluQueryPosition = "0";
        #endregion


        #region Hulu Constant Params
        private const string HuluParamVideoType = "episode";
        private const string HuluParamCategories = "Episodes";
        private const string HuluParamIncludeNonBrowseable = "1";
        private const string HuluParamFreeOnly = "1";
        private const string HuluParam_Lang = "en";
        private const string HuluParam_Region = "us";
        private const string HuluParamItemsPerPage = "100";
        private const string HuluParamUserPgId = "1";
        private const string HuluParamContentPgId = "963";
        private const string HuluParamDeviceId = "1";
        private const string HuluParamRegion = "us";
        private const string HuluParamLocale = "en";
        private const string HuluParamLang = "en";
        private const string HuluParamDonutScope = "h2o%3Aus";
        private const string HuluParamTreatment = "control";
        private const string HuluParamZzUserId = "56592346";
        private const string HuluParamHis = "";
        private const string HuluParamWhis = "";
        #endregion

        private static string ConstructHuluEpisodesPrimaryUrl()
        {
            return HuluBaseUrl + HuluEpisodesPath1 + "categories=" + HuluParamCategories +
                   "&free_only=" + HuluParamFreeOnly + "&include_nonbrowseable=" + HuluParamIncludeNonBrowseable +
                   "&show_id=" + HuluShowId + "&sort=" + HuluParamSort +
                   "&_language=" + HuluParam_Lang + "&_region=" + HuluParam_Region +
                   "&items_per_page=" + HuluParamItemsPerPage + "&position=" + HuluQueryPosition +
                   "&_user_pgid=" + HuluParamUserPgId + "&_content_pgid=" + HuluParamContentPgId +
                   "&_device_id=" + HuluParamDeviceId + "&region=" + HuluParamRegion +
                   "&locale=" + HuluParamLocale + "&language=" + HuluParamLang +
                   "&access_token=" + HuluParamAccessToken;
        }

        private static string ConstructHuluEpisodesSecondaryUrl()
        {
            return HuluBaseUrl + HuluEpisodesPath2.Replace(":showId", HuluShowId) + "free_only=" + HuluParamFreeOnly +
                   "&include_nonbrowseable=" + HuluParamIncludeNonBrowseable + "&show_id=" + HuluShowId +
                   "&sort=" + HuluParamSort + "&video_type=" + HuluParamVideoType +
                   "&_language=" + HuluParam_Lang + "&_region=" + HuluParam_Region +
                   "&items_per_page=" + HuluParamItemsPerPage + "&position=" + HuluQueryPosition +
                   "&_user_pgid=" + HuluParamUserPgId + "&_content_pgid=" + HuluParamContentPgId +
                   "&_device_id=" + HuluParamDeviceId + "&region=" + HuluParamRegion +
                   "&locale=" + HuluParamLocale + "&language=" + HuluParamLang +
                   "&access_token=" + HuluParamAccessToken;
        }

        private static string ConstructHuluSeriesUrl()
        {
            return HuluBaseUrl + HuluSeriesPath.Replace(":companyId", HuluNetworkId) + "sort=" + HuluParamSort +
                   "&_language=" + HuluParam_Lang + "&_region=" + HuluParam_Region +
                   "&items_per_page=" + HuluParamItemsPerPage + "&position=" + HuluQueryPosition +
                   "&_user_pgid=" + HuluParamUserPgId + "&_content_pgid=" + HuluParamContentPgId +
                   "&_device_id=" + HuluParamDeviceId + "&region=" + HuluParamRegion +
                   "&locale=" + HuluParamLocale + "&language= " + HuluParamLang +
                   "&access_token=" + HuluParamAccessToken;
        }

        private static string ConstructWhiteListHuluNetworksUrl()
        {
            return HuluBaseUrl + HuluNetworksPath + "mode=" + HuluParamMode +
                   "&sort=" + HuluParamSort + "&_language=" + HuluParam_Lang +
                   "&_region=" + HuluParam_Region + "&items_per_page=" + HuluParamItemsPerPage +
                   "&position=" + HuluQueryPosition + "&_user_pgid=" + HuluParamUserPgId +
                   "&_content_pgid=" + HuluParamContentPgId + "&_device_id=" + HuluParamDeviceId +
                   "&region=" + HuluParamRegion + "&locale=" + HuluParamLocale +
                   "&language= " + HuluParamLang + "&access_token=" + HuluParamAccessToken;
        }

        private static string ConstructAlternateHuluWhiteListNetworksUrl()
        {
            return HuluBaseUrl + HuluNetworksPath + "show_id=" + HuluShowId + "&_language=" + HuluParam_Lang +
                   "&_region=" + HuluParam_Region + "&items_per_page=" + HuluParamItemsPerPage +
                   "&position=" + HuluQueryPosition + "&_user_pgid=" + HuluParamUserPgId +
                   "&_content_pgid=" + HuluParamContentPgId + "&_device_id=" + HuluParamDeviceId +
                   "&region=" + HuluParamRegion + "&locale=" + HuluParamLocale +
                   "&language= " + HuluParamLang + "&donut_scope" + HuluParamDonutScope +
                   "&treatment=" + HuluParamTreatment + "&zzuser_id=" + HuluParamZzUserId +
                   "&access_token=" + HuluParamAccessToken;
        }

        public static void GetHuluAccessToken(bool force = false, WebProxy proxy = null)
        {
            if (HuluParamAccessToken != "" && !force)
            {
                var timeElapsed = DateTime.Now - timeReceivedAccessToken;
                if (timeElapsed.Minutes < 10)
                    return;
            }
            var req = (HttpWebRequest)WebRequest.Create(HuluBaseUrl);
            if (_proxy != null)
                req.Proxy = _proxy;
            var resp = (HttpWebResponse)req.GetResponse();
            var html = new StreamReader(resp.GetResponseStream(), Encoding.Default).ReadToEnd();
            var tokenArea = html.Substring(html.IndexOf("API_DONUT", System.StringComparison.Ordinal));
            var token = tokenArea.Split(';').ElementAt(0)
                .Split('=').ElementAt(1)
                .Replace(" \'", "")
                .Replace("\'", "");
            HuluParamAccessToken = token;
            timeReceivedAccessToken = DateTime.Now;
        }

        public static void SetWebProxy(WebProxy proxy)
        {
            _proxy = proxy;
        }

        private static string GetHuluJsonData(string url, out TimeSpan timeSpan)
        {
            var start = new DateTime();
            try
            {
                _webRequest = (HttpWebRequest)WebRequest.Create(url);
                if (_proxy != null)
                    _webRequest.Proxy = _proxy;

                // track time taken to use proxy
                start = DateTime.Now;
                _webResponse = (HttpWebResponse)_webRequest.GetResponse();
                var end = DateTime.Now;
                timeSpan = end - start;

                _reader = new StreamReader(_webResponse.GetResponseStream(), Encoding.Default);
                _proxy = null;

                return _reader.ReadToEnd();
            }
            catch (Exception)
            {
                var end = DateTime.Now;
                timeSpan = end - start;
                return null;
            }
        }

        public static void MakeCallToHuluApi(int selection, out string json, out TimeSpan timeSpan)
        {
            GetHuluAccessToken();
            switch (selection)
            {
                case 0:
                    json = GetHuluJsonData(ConstructHuluEpisodesPrimaryUrl(), out timeSpan);
                    if (!ValidateJson(json))
                        json = GetHuluJsonData(ConstructHuluEpisodesSecondaryUrl(), out timeSpan);
                    return;
                case 1:
                    json = GetHuluJsonData(ConstructHuluSeriesUrl(), out timeSpan);
                    return;
                case 2:
                    json = GetHuluJsonData(ConstructWhiteListHuluNetworksUrl(), out timeSpan);
                    return;
            }
            throw new Exception("Failed To Make Call To Hulu API");
        }

        private static bool ValidateJson(string json)
        {
            var jObject = JObject.Parse(json);
            var count = jObject["total_count"].Value<int>();
            return count > 0;
        }
    }
}
