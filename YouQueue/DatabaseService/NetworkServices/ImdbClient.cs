﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;
using YouQueueService.Entities;

namespace YouQueueService.NetworkServices
{
    public class ImdbClient
    {
        private static HttpWebRequest _webRequest;
        private static HttpWebResponse _webResponse;
        private static Uri _imdbUri;
        private static Uri _baseUri = new Uri("http://www.omdbapi.com/");
        
        public static void GetSeriesData(ref Series series)
        {
            var seriesData = GetDataById(FindSeriesByName(series.SeriesName));

            if (seriesData == null)
                return;

            series.Genre = seriesData["Genre"];
            series.Year = seriesData["Year"];
        }

        private static string FindSeriesByName(string seriesName)
        {
            _imdbUri = new Uri(_baseUri, "?s=" + seriesName);
            _webRequest = (HttpWebRequest)WebRequest.Create(_imdbUri);
            _webResponse = (HttpWebResponse)_webRequest.GetResponse();
            string str = new StreamReader(_webResponse.GetResponseStream(), Encoding.Default).ReadToEnd();
            if (str.Contains("Error"))
                return "";
            JObject o = JObject.Parse(str);
            o = o["Search"]
                .Where(j => String.Equals(j["Title"].Value<string>(), seriesName, StringComparison.CurrentCultureIgnoreCase))
                .ElementAt(0) as JObject;
            return o["imdbID"].Value<string>();
        }

        private static Dictionary<string, string> GetDataById(string id)
        {
            if (id == "")
                return null;
            _imdbUri = new Uri(_baseUri, "?i=" + id);
            _webRequest = (HttpWebRequest)WebRequest.Create(_imdbUri);
            _webResponse = (HttpWebResponse)_webRequest.GetResponse();
            string str = new StreamReader(_webResponse.GetResponseStream(), Encoding.Default).ReadToEnd();
            JObject o = JObject.Parse(str);
            var data = new Dictionary<string, string>();
            data.Add("Genre", o["Genre"].Value<string>());
            data.Add("Year", RemoveGarbageFromString(o["Year"].Value<string>()));
            return data;
        }

        private static string RemoveGarbageFromString(string str)
        {
            return str.Contains("â€“") ? str.Replace("â€“", "-") : str;
        }
    }
}
