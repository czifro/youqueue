﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouQueueService.Entities;

namespace YouQueueService
{
    public static class Constants
    {
        public static List<Proxy> Proxies = new List<Proxy>();

        public static void LoadConstants(YouQueueContext context)
        {
            Proxies.AddRange(context.Proxies
                .OrderByDescending(x => x.AverageDelayTime)
                .Select(p => new Proxy
                { 
                    ProxyId = p.ProxyId,
                    AnonymityLevel = p.AnonymityLevel,
                    BlackListed = p.BlackListed
                }));
        }
    }
}
