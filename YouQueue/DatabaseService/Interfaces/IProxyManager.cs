﻿using System.Collections.Generic;
using System.Threading.Tasks;
using YouQueueService.Entities;

namespace YouQueueService.Interfaces
{
    interface IProxyManager
    {
        Task<double> TestProxiesAsync();

        double TestProxies();

        IDictionary<string, List<Proxy>> GetTopProxies();
    }
}
