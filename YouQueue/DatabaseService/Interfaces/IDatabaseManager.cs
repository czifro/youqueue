﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouQueueService.Entities;

namespace YouQueueService.Interfaces
{
    public interface IDatabaseManager
    {
        void UploadEpisodes(List<Episode> episodes);

        void UploadSeries(List<Series> series);

        void UploadNetworks(List<Network> networks);

        void RemoveNetwork(string url);

        void RemoveSeries(string url);

        void DbCleanup();

        void SaveAndDispose();
    }
}
