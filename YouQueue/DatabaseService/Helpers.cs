﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using YouQueueService.Entities;

namespace YouQueueService.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime ToDateTime(string dateTimeString)
        {
            return dateTimeString == null ? new DateTime(1992, 12, 8) : DateTime.Parse(dateTimeString);
        }

        public static string ToString(DateTime dateTime)
        {
            return dateTime.Date.ToString("D");
        }
    }

    public static class ProxyHelper
    {
        public static void GenerateAndInsertProxyLog(Proxy proxy, TimeSpan delay, bool hasSucceeded)
        {
            //var context = new YouQueueContext();
            //var pr = context.Proxies.Single(p => p.IPAddress == proxy.IPAddress);
            var proxyLog = new ProxyLog
            {
                DelayTime = delay.Milliseconds,
                ProxyId = proxy.ProxyId,
                DidSucceed = Convert.ToInt32(hasSucceeded)
            };
            var pl = proxyLog;
            //context.ProxyLogs.Add(proxyLog);
            //context.SaveChanges();
        }

        public static WebProxy ToWebProxy(Proxy pr)
        {
            WebProxy webProxy;
            try
            {
                webProxy = new WebProxy(pr.IPAddress, pr.Port);
                if (pr.UserName != null && pr.Password != null)
                {
                    webProxy.Credentials = pr.Domain != null
                        ? new NetworkCredential(pr.UserName, pr.Password)
                        : new NetworkCredential(pr.UserName, pr.Password, pr.Domain);
                }
            }
            catch (Exception)
            {
                return null;
            }
            return webProxy;
        }

        public static Proxy ToProxy(WebProxy wpr)
        {
            return new Proxy
            {
                IPAddress = wpr.Address.Host
            };
        }
    }
}
