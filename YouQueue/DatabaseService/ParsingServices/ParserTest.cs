﻿using System.Linq;
using YouQueueService.NetworkServices;
using YouQueueService.ParsingServices.ParseHtml;
using YouQueueService.ParsingServices.ParseJson;
using YouQueueService;
using YouQueueService.Entities;
using YouQueueService.NetworkServices;


namespace YouQueueService.ParsingServices
{
    //[TestClass]
    public class ParserTest
    {
        public void ExtractCwDetailsTest()
        {
            var webPageDownloader = new WebPageDownloader();
            var htmlParser = new HtmlParser();
            var currentNetwork = new Network { NetworkId = 0, NetworkName = "The CW", HuluNetworkId = 446 };

            #region Code for old method
            //htmlParser.CurrentNetwork = currentNetwork;
            //var html = webPageDownloader.GetHtmlAsString(currentNetwork.Url);
            //htmlParser.Parse(html, false);

            //htmlParser.CurrentSeries = htmlParser.SeriesList.ElementAt(0);
            //html = (htmlParser.CurrentNetwork.NetworkName == "TBS" ?
            //    webPageDownloader.XmlHttpRequest("http://www.tbs.com/video/content/services/getCollectionByContentIdNew.do?test=2226666&offset=0&sort=&cTest=2&limit=200&id=446550") :
            //    webPageDownloader.GetHtmlAsString(htmlParser.CurrentSeries.SeriesUrl));

            //htmlParser.Parse(html);
            #endregion

            var huluParser = new HuluParser { CurrentNetwork = currentNetwork, CurrentSeries = new Series { SeriesId = 0 } };
            var html = webPageDownloader.GetHtmlAsString("http://www.hulu.com");
            htmlParser.GetHuluAccessToken(html);
            HuluClient.HuluParamSort = "popular_today";
            HuluClient.HuluNetworkId = huluParser.CurrentNetwork.HuluNetworkId.ToString();
            //var json = HuluClient.GetHuluJsonData(Constants.ConstructHuluSeriesUrl());

            //huluParser.Parse(json, 2);
            //if (!huluParser.Parse(json, 1))
            //{
            //    json = HuluClient.GetNetworkAsJson(Constants.ConstructHuluEpisodesSecondaryUrl());
            //    huluParser.Parse(json, 1);
            //}
        }
    }
}
