﻿using System.Web.Script.Serialization;
using YouQueueService.Entities;

namespace YouQueueService.ParsingServices.ToJson
{
    class ParseToJson
    {
        private readonly JavaScriptSerializer _serializer;

        public ParseToJson()
        {
            _serializer = new JavaScriptSerializer();
        }

        public string ConvertSeriesUrls(Series series)
        {
            return _serializer.Serialize(series);
        }

        public string ConvertNetworkUrls(Network network)
        {
            return _serializer.Serialize(network);
        }
    }
}
