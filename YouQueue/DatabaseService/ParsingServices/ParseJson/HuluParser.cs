﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json.Linq;
using YouQueueService.Entities;
using YouQueueService.NetworkServices;

namespace YouQueueService.ParsingServices.ParseJson
{
    public class HuluParser
    {
        private JObject _jObject;
        private JToken _jToken;
        public Network CurrentNetwork { get; set; }
        public Series CurrentSeries { get; set; }
        public List<Network> NetworkList { get; set; }
        public List<Series> SeriesList { get; set; }
        public List<Episode> EpisodesList { get; set; }

        public HuluParser()
        {
            SeriesList = new List<Series>();
            EpisodesList = new List<Episode>();
            NetworkList = new List<Network>();
        }

        public bool Parse(string json, int selection)
        {
            _jObject = JObject.Parse(json);
            if (_jObject["total_count"].Value<int>() == 0) return false;
            _jToken = _jObject["data"];

            var jenum = _jToken.Children().ToList();
            switch (selection)
            {
                case 1:
                    foreach (var j in jenum)
                    {
                        ExtractEpisodes(JObject.Parse(j["video"].ToString()));
                    }
                    if (!EpisodesList.Any()) return false;
                    break;
                case 2:
                    foreach (var j in jenum)
                    {
                        ExtractSeries(JObject.Parse(j["show"].ToString()));
                    }
                    if (!SeriesList.Any()) return false;
                    break;
                case 3:
                    foreach (var j in jenum)
                    {
                        ExtractNetwork(JObject.Parse(j["company"].ToString()));
                    }
                    if (!NetworkList.Any()) return false;
                    break;
            }
            return true;
        }

        private void ExtractEpisodes(IDictionary<string, JToken> jObject)
        {
            if (!jObject["programming_type"].Value<string>().Equals("Full Episode"))
                return;
            try
            {
                var e = new Episode
                {
                    EpisodeName = jObject["title"].Value<string>(),
                    SeasonNum = jObject["season_number"].Value<int>(),
                    EpisodeNum = jObject["episode_number"].Value<int>(),
                    Description = jObject["description"].Value<string>(),
                    ExpirationDate = jObject["expires_at"].Value<string>(),
                    SeriesId = CurrentSeries.SeriesId,
                    Thumbnail = jObject["thumbnail_url"].Value<string>()
                };
                EpisodesList.Add(e);
            }
            catch (Exception)
            {
                Trace.TraceError("Failed To Parse Episode");
            }
            
        }

        private void ExtractSeries(IDictionary<string, JToken> jObject)
        {
            if (jObject["episodes_count"].Value<int>() == 0) return;
            try
            {
                var s = new Series
                    {
                        SeriesName = jObject["name"].Value<string>().Replace(" (TV)", ""),
                        HuluShowId = jObject["id"].Value<int>(),
                        NetworkId = CurrentNetwork.NetworkId,
                        Thumbnail = jObject["thumbnail_url"].Value<string>().Replace("\\", "")
                    };

                ImdbClient.GetSeriesData(ref s);

                SeriesList.Add(s);
            }
            catch (Exception)
            {
                Trace.TraceError("Failed To Parse Series");
            }
        }

        private void ExtractNetwork(IDictionary<string, JToken> jObject)
        {
            var n = new Network
            {
                NetworkName = jObject["name"].Value<string>(),
                HuluNetworkId = jObject["id"].Value<int>(),
                Thumbnail = jObject["key_art_url"].Value<string>().Replace("\\", "")
            };

            NetworkList.Add(n);
        }
    }
}
