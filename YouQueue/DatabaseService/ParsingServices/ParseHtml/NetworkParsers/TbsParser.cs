﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;
using HtmlAgilityPack;
using YouQueueService.Entities;
using YouQueueService.NetworkServices;
using YouQueueService.NetworkServices;

namespace YouQueueService.ParsingServices.ParseHtml.NetworkParsers
{
    class TbsParser
    {

        private readonly HtmlDocument _doc;
        private string _oldPath = "/index.jsp?cid=";

        private string _newPath =
            "/content/services/getColectionByContentIdNew.do?test=2226666&offset=0&sort=&cTest=2&limit=200&id=";
        private HtmlNodeCollection _nodes;
        public string Html { get; set; }
        public List<Series> SeriesList { get; private set; }
        public Series CurrentSeries { get; set; }
        public Network CurrentNetwork { get; set; }
        public List<Episode> EpisodesForCurrentSeries { get; private set; }

        public TbsParser()
        {
            _doc = new HtmlDocument();
            SeriesList = new List<Series>();
            EpisodesForCurrentSeries = new List<Episode>();
        }

        public void ParseTbsList()
        {
            _doc.LoadHtml(Html);
            _doc.LoadHtml(_doc.DocumentNode.SelectSingleNode("//ul[@id=\"shows_subnav\"]").InnerHtml);

            _nodes = _doc.DocumentNode.SelectNodes("//a[contains(@href, \"?cid=\")]");

            foreach (var node in _nodes)
            {
                try
                {
                    string link = node.Attributes["href"].Value.Replace(_oldPath, _newPath);
                    var series = new Series
                    {
                        Network = CurrentNetwork,
                        SeriesUrl = link,
                        SeriesName = node.InnerText
                    };
                    if (series.SeriesName.ToLower() != "movies")
                    {
                        GetThumbnailTbsShowsLite(series.SeriesName, ref series);
                        ImdbClient.GetSeriesData(ref series);
                    }
                    SeriesList.Add(series);
                }
                catch (Exception)
                {
                    Console.WriteLine("Failed to extract series from node");
                }
            }
        }

        private void GetThumbnailTbsShowsLite(string seriesName, ref Series series)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(Html);
            var img = doc.DocumentNode.Descendants("img")
                .Where(n => n.Attributes.Contains("alt") && n.Attributes["alt"].Value.ToLower() ==
                      (seriesName.Contains("\'") ? seriesName.Replace("\'", "&#039;") : seriesName).ToLower())
                      .Select(n => n);
            if (!img.Any()) // last resort if thumbnail can't be found
            {
                GetThumbnailTbsShowsSlowSearch(seriesName, series);
                return;
            }

            series.SeriesName = img.ElementAt(0).Attributes["alt"].Value.Replace("&#039;", "\'");
            series.Thumbnail = img.ElementAt(0).Attributes["src"].Value;
        }

        private void GetThumbnailTbsShowsSlowSearch(string seriesName, Series series)
        {
            const string pattern = @"\b([a-z]\w+[:][/][/][a-zA-z0-9]+[.a-zA-z0-9]+[.a-zA-z0-9]+[.a-zA-z0-9]+)([/][a-zA-z0-9]+)+([.][j][p][g])";
            var reg = new Regex(@pattern);
            var matches = reg.Matches(Html);
            var links = new Match[matches.Count];
            matches.CopyTo(links, 0);
            var thumbLink = links.Where(m => m.Groups[0].Value.ToLower().Contains(seriesName.Split(' ').ElementAt(0).ToLower()));
            series.Thumbnail = thumbLink.ElementAt(0).Value;
        }

        public void ParseTbsEpisodes()
        {
            var xPathDocument = new XPathDocument(XmlReader.Create(new StringReader(Html)));
            var nav = xPathDocument.CreateNavigator();
            var nodes = nav.Select("//episode");
            int nodeCount = nodes.Count;
            ExtractEpisodeData(nodes.Current, nodeCount--);
            while (nodes.MoveNext())
                ExtractEpisodeData(nodes.Current, nodeCount--);
        }

        private void ExtractEpisodeData(XPathNavigator nav, int count)
        {

        }

    }
}
