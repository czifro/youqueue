﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using YouQueueService;
using YouQueueService.Entities;
using YouQueueService.NetworkServices;
using YouQueueService.NetworkServices;

namespace YouQueueService.ParsingServices.ParseHtml.NetworkParsers
{
    class CwParser
    {
        private readonly HtmlDocument _doc;
        private HtmlNodeCollection _nodes;
        public List<Series> SeriesList { get; private set; }
        public Series CurrentSeries { get; set; }
        public Network CurrentNetwork { get; set; }
        public List<Episode> EpisodesForCurrentSeries { get; private set; }

        public CwParser()
        {
            _doc = new HtmlDocument();
            SeriesList = new List<Series>();
            EpisodesForCurrentSeries = new List<Episode>();
        }

        public void ParseCwList()
        {
            _doc.LoadHtml(_doc.DocumentNode.SelectSingleNode("//div[@class=\"shows-current\"]").InnerHtml);

            _nodes = _doc.DocumentNode.SelectNodes("//a[@href]");

            foreach (var node in _nodes)
            {
                var s = new Series
                {
                    SeriesName = node.ChildNodes["p"].InnerText,
                    SeriesUrl = "https://www.cwtv.com/" + node.Attributes["href"]
                        .Value.Replace("/shows", "cw-video") + "/full-episodes/",
                    Network = CurrentNetwork,
                    Thumbnail = node.ChildNodes["div"].ChildNodes["img"].Attributes["src"].Value
                };
                ImdbClient.GetSeriesData(ref s);
                SeriesList.Add(s);
            }
        }

        public void ParseCwEpisodes()
        {
            if (EpisodesForCurrentSeries.Any())
                EpisodesForCurrentSeries = new List<Episode>();
            _nodes = _doc.DocumentNode.SelectNodes("//div[contains(@class,\"videowrapped slide full-episodes\")]");
            var weekCount = (ExtractCwShowData(_nodes[(_nodes.Count - 1)], 0) == null ? (_nodes.Count - 1) : _nodes.Count);
            foreach (var node in _nodes)
            {
                var epi = ExtractCwShowData(node, weekCount--);
                if (epi == null)
                {
                    ++weekCount;
                    continue;
                }
                EpisodesForCurrentSeries.Add(epi);
            }
        }

        private Episode ExtractCwShowData(HtmlNode node, int weeksLeft)
        {
            var noSeason = false;
            var epiNum = 0;
            var thumbnail = node.ChildNodes["a"].ChildNodes[1].ChildNodes["img"];
            var details = node.ChildNodes["a"].ChildNodes[5];
            if (!details.ChildNodes[3].InnerText.Split('O').ElementAt(0).Contains("Ep."))
                return null;
            if (!details.ChildNodes[3].InnerText.Split('O').ElementAt(0).Contains("Season"))
            {
                noSeason = true;
                epiNum = Convert.ToInt32(
                    details.ChildNodes[3].InnerText.Split('O').ElementAt(0).Substring(3));
            }

            var temp = new Episode
            {
                Thumbnail = thumbnail.Attributes["src"].Value,
                Description = details.ChildNodes[5].InnerText,
                EpisodeName = details.ChildNodes[1].InnerText,
                Series = CurrentSeries,
                //WeeksLeft = weeksLeft,           // Todo: fix
                EpisodeNum = noSeason ? epiNum : Convert.ToInt32(
                    details.ChildNodes[3].InnerText.Split('O').ElementAt(0).Split(',').ElementAt(1).Substring(5)),
                SeasonNum = noSeason ? 0 : Convert.ToInt32(
                    details.ChildNodes[3].InnerText.Split('O').ElementAt(0).Split(',').ElementAt(0).Substring(7))
            };
            return temp;
        }

    }
}
