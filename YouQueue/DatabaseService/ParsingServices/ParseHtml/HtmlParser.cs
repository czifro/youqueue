﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using YouQueueService.Entities;
using YouQueueService.NetworkServices;
using YouQueueService.ParsingServices.ParseHtml.NetworkParsers;

namespace YouQueueService.ParsingServices.ParseHtml
{
    public class HtmlParser
    {
        private readonly HtmlDocument _doc;
        private HtmlNodeCollection _nodes;
        private string _html;
        public List<Series> SeriesList { get; private set; }
        public Series CurrentSeries { get; set; }
        public Network CurrentNetwork { get; set; }
        public List<Episode> EpisodesForCurrentSeries { get; private set; }

        public HtmlParser()
        {
            _doc = new HtmlDocument();
            SeriesList = new List<Series>();
            EpisodesForCurrentSeries = new List<Episode>();
        }

        public void Parse(string html, bool getEpisodes = true)
        {
            _html = html;
            if (CurrentNetwork == null || string.IsNullOrEmpty(CurrentNetwork.NetworkName))
                throw new ArgumentNullException("CurrentNetwork");
            switch (CurrentNetwork.NetworkName)
            {
                case "The CW":
                    Console.WriteLine("Parsing The CW...");
                    _doc.LoadHtml(_html);
                    var cwParser = new CwParser { CurrentNetwork = CurrentNetwork };
                    if (getEpisodes)
                    {
                        cwParser.CurrentSeries = CurrentSeries;
                        cwParser.ParseCwEpisodes();
                    }
                    break;
                case "TBS":
                    Console.WriteLine("Parsing TBS...");
                    _doc.LoadHtml(_html);
                    var tbsParser = new TbsParser { CurrentNetwork = CurrentNetwork, Html = _html };
                    if (getEpisodes)
                    {
                        tbsParser.CurrentSeries = CurrentSeries;
                        tbsParser.ParseTbsEpisodes();
                        EpisodesForCurrentSeries = tbsParser.EpisodesForCurrentSeries;
                    }
                    else
                    {
                        tbsParser.ParseTbsList();
                        SeriesList = tbsParser.SeriesList;
                    }
                    break;
            }
        }

        public void GetHuluAccessToken(string html)
        {
            var tokenArea = html.Substring(html.IndexOf("API_DONUT"));
            var token = tokenArea.Split(';').ElementAt(0)
                .Split('=').ElementAt(1)
                .Replace(" \'", "")
                .Replace("\'", "");
            HuluClient.HuluParamAccessToken = token;
        }
    }
}
