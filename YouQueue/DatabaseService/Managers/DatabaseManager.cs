﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin.Security.Provider;
using YouQueueService.Entities;
using YouQueueService.Interfaces;

namespace YouQueueService.Manager
{
    public class DatabaseManager : IDatabaseManager
    {
        private YouQueueContext _context;
        private bool disposed = false;
        public List<string> BrokenNetworkUrlsList { get; set; }

        public List<Network> BufferedNetworks = new List<Network>();
        public List<Series> BufferedSeries = new List<Series>();
        public List<Episode> BufferedEpisodes = new List<Episode>();

        public DatabaseManager(ref YouQueueContext context)
        {
            _context = context;
        }

        public void UploadEpisodes(List<Episode> episodes)
        {
            BufferedEpisodes.AddRange(SeparateNewEpisodes(episodes.AsEnumerable()));
        }

        public void UploadSeries(List<Series> series)
        {
            BufferedSeries.AddRange(SeperateNewSeries(series));
        }

        public void UploadNetworks(List<Network> networks)
        {
            BufferedNetworks.AddRange(SeparateNewNetworks(networks));
        }

        public void RemoveNetwork(string url)  // todo: figure out better method
        {
            if (string.IsNullOrEmpty(url))
                throw new ArgumentNullException("url");
            var network = _context.Networks.SingleOrDefault(n => n.Url == url);
            var series = _context.Series.Where(s => s.Network.NetworkId == network.NetworkId).AsEnumerable();
            var episodes = _context.Episodes.Join(_context.Series, e => e.Series.SeriesId, s => s.SeriesId, (e, s) => new { e, s })
                .Where(@t => @t.s.Network.NetworkId == network.NetworkId)
                .Select(@t => @t.e);
            var uqitems = _context.QueueItems.Join(_context.Episodes, i => i.Episode.EpisodeId, e => e.EpisodeId, (i, e) => new { i, e })
                .Join(_context.Series, @t => @t.e.Series.SeriesId, sr => sr.SeriesId, (@t, sr) => new { @t, sr })
                .Select(@t => @t.@t.i);
            if (uqitems.Any()) _context.QueueItems.RemoveRange(uqitems);
            if (episodes.Any()) _context.Episodes.RemoveRange(episodes);
            if (series != null) _context.Series.RemoveRange(series);
            if (network != null) _context.Networks.Remove(network);
            _context.SaveChanges();
        }

        public void RemoveSeries(string url)  // todo: figure out better method
        {
            var series = _context.Series.SingleOrDefault(s => s.SeriesUrl == url);
            var episodes = _context.Episodes.Join(_context.Series, e => e.Series.SeriesId, s => s.SeriesId, (e, s) => new { e, s })
                .Where(@t => @t.e.Series.SeriesId == series.SeriesId)
                .Select(@t => @t.e);
            var uqitems = _context.QueueItems.Join(_context.Episodes, i => i.Episode.EpisodeId, e => e.EpisodeId, (i, e) => new { i, e })
                .Join(_context.Series, @t => @t.e.Series.SeriesId, sr => sr.SeriesId, (@t, sr) => new { @t, sr })
                .Select(@t => @t.@t.i);
            if (uqitems.Any()) _context.QueueItems.RemoveRange(uqitems);
            if (episodes.Any()) _context.Episodes.RemoveRange(episodes);
            if (series != null) _context.Series.Remove(series);
            _context.SaveChanges();
        }

        public void DbCleanup()
        {
            // Delete checked off items
            var deletedUserItems = _context.QueueItems.Where(item => item.DeleteItem == 1);
            if (deletedUserItems.Any())
                _context.QueueItems.RemoveRange(deletedUserItems);
            // Delete old items
            var oldEpisodes = _context.Episodes.Where(item => item.ExpirationDate != null &&
                                                      DateTime.Parse(item.ExpirationDate) < DateTime.UtcNow);
            CreateNotifications();
            if (oldEpisodes.Any())
                _context.Episodes.RemoveRange(oldEpisodes);
        }

        

        private void CreateNotifications()
        {
            var data =
                _context.QueueItems.Join(_context.Episodes, u => u.Episode.EpisodeId, e => e.EpisodeId,
                    (u, e) => new {u, e})
                    .Join(_context.Series, @s => @s.e.Series.SeriesId, sr => sr.SeriesId, (@s, sr) => new {@s, sr})
                    .Where(epi => DateTime.Parse(epi.@s.e.ExpirationDate) < DateTime.UtcNow && epi.@s.u.DeleteItem == 0)
                    .AsQueryable();
            foreach (var d in data)
            {
                var msg = "Oops, you missed an episode! " + d.s.e.EpisodeName + " from " + d.sr.SeriesName +
                             " has expired!";
                _context.Notifications.Add(new Notification { User = d.s.u.User, Timestamp = DateTime.UtcNow, Message = msg });
            }
        }

        private List<Episode> SeparateNewEpisodes(IEnumerable<Episode> episodes)
        {
            var newList = new List<Episode>();
            if (!episodes.Any())
                return newList;
            var tEpi = episodes.ElementAt(0);
            var episodesInDb = _context.Episodes.Where(e => e.Series.SeriesId == tEpi.Series.SeriesId);
            if (!episodesInDb.Any()) return episodes.ToList();
            foreach (var e in episodes)
            {
                var temp = e;
                var episodeToUpdate =
                    episodesInDb.SingleOrDefault(x => x.EpisodeName.Equals(temp.EpisodeName));
                if (episodeToUpdate == null) newList.Add(e);
                else
                {
                    episodeToUpdate.ExpirationDate = e.ExpirationDate;
                }
            }

            return newList;
        }

        private List<Series> SeperateNewSeries(List<Series> series)
        {
            var newList = new List<Series>();
            if (!series.Any())
                return newList;
            var tSeries = series.ElementAt(0);
            var seriesInDb = _context.Series.Where(s => s.Network.NetworkId == tSeries.Network.NetworkId);
            if (!seriesInDb.Any()) return series;

            foreach (var s in series)
            {
                var temp = s;
                var temp1 = seriesInDb.Where(x => x.SeriesName.Equals(temp.SeriesName));
                if (!temp1.Any())
                    newList.Add(s);
            }
            return newList;
        }

        private List<Network> SeparateNewNetworks(List<Network> networks)
        {
            var newList = new List<Network>();  // todo: rewrite
            if (!networks.Any())
                return newList;
            var networksInDb = _context.Networks.Select(n => n);
            if (!networksInDb.Any()) return networks;
            foreach (var network in networks)
            {
                var temp = network;
                var temp1 = networksInDb
                    .Where(x => x.NetworkName.Equals(temp.NetworkName));
                if (!temp1.Any())
                    newList.Add(network);
            }

            return newList;
        }
        
        public void SaveAndDispose()
        {
            if (disposed)
                return;
            _context.Episodes.AddRange(BufferedEpisodes);
            _context.Series.AddRange(BufferedSeries);
            _context.Networks.AddRange(BufferedNetworks);
            _context.SaveChanges();
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            BufferedEpisodes = null;
            BufferedSeries = null;
            BufferedNetworks = null;
            _context.Dispose();
            _context = null;
        }
    }
}
