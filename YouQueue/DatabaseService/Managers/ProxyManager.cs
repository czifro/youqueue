﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouQueueService.Entities;
using YouQueueService.Helpers;
using YouQueueService.Interfaces;
using YouQueueService.NetworkServices;

namespace YouQueueService.Managers
{
    public class ProxyManager : IProxyManager
    {
        public Task<double> TestProxiesAsync()
        {
            return new Task<double>(TestProxies);
        }

        public double TestProxies()
        {
            var context = new YouQueueContext();
            var proxies = context.Proxies.Select(n => n);
            var succesCount = 0;
            foreach (var proxy in proxies)
            {
                var webProxy = ProxyHelper.ToWebProxy(proxy);
                HuluClient.SetWebProxy(webProxy);
                string json;
                var start = new DateTime();
                var timeSpan = new TimeSpan(0, 0, 0, 0, 0);
                var end = new DateTime();
                try
                {
                    if (proxy.AnonymityLevel == "Transparent")
                    {
                        start = DateTime.Now;
                        HuluClient.GetHuluAccessToken(true, webProxy);
                        end = DateTime.Now;
                        timeSpan = end - start;
                    }
                    else
                    {
                        HuluClient.SetWebProxy(webProxy);
                        HuluClient.MakeCallToHuluApi(2, out json, out timeSpan);
                    }
                    ProxyHelper.GenerateAndInsertProxyLog(proxy, timeSpan, true);
                    ++succesCount;
                }
                catch (Exception)
                {
                    if (timeSpan.Milliseconds == 0)
                    {
                        end = DateTime.Now;
                        timeSpan = end - start;
                        ProxyHelper.GenerateAndInsertProxyLog(proxy, timeSpan, false);
                    }
                    else
                        ProxyHelper.GenerateAndInsertProxyLog(proxy, timeSpan, false);
                }
            }
            return (double)succesCount / proxies.Count();
        }

        public IDictionary<string, List<Proxy>> GetTopProxies()
        {

            return null;
        }

        private async Task<List<Proxy>> GetTopProxiesAsync(List<Proxy> proxies)
        {
            if (proxies == null)
                return null;

            var usageProxies = proxies;
            var successRateProxies = proxies;

            

            successRateProxies = await CalculateSuccessRateAsync(successRateProxies);
            usageProxies = await CalculateUsageAsync(usageProxies);

            List<Proxy> topProxies = successRateProxies.Union(usageProxies).ToList();

            return topProxies;
        }

        private Task<List<Proxy>> CalculateSuccessRateAsync(List<Proxy> proxies)
        {
            foreach (var p in proxies)
            {
                if (p.ProxyLogs == null || p.ProxyLogs.Count == 0)
                {
                    p.TempValue = 0.0;
                    continue;
                }
                p.TempValue = p.ProxyLogs.Count(x => x.DidSucceed == 1)/p.ProxyLogs.Count;
            }
            var selectProxies = proxies.OrderBy(x => x.TempValue)
                .ToList()
                .GetRange((int)(proxies.Count*0.6), (int)(proxies.Count*0.8));
            return Task.FromResult(selectProxies);
        }

        private Task<List<Proxy>> CalculateUsageAsync(List<Proxy> proxies)
        {
            return Task.FromResult(proxies.OrderBy(x => x.ProxyLogs.Count)
                                          .ToList()
                                          .GetRange((int)(proxies.Count*0.6), (int)(proxies.Count*0.8)));
        }
    }
}
