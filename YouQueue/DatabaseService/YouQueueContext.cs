﻿using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using YouQueueService.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace YouQueueService
{
    public class YouQueueContext : IdentityDbContext<YouQueueService.Entities.ApplicationUser>
    {
        public YouQueueContext()
            : base("Staging", throwIfV1Schema: false)
        {
        }

        public static YouQueueContext Create()
        {
            return new YouQueueContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);


        }

        public DbSet<Network> Networks { get; set; }
        public DbSet<Series> Series { get; set; }
        public DbSet<Episode> Episodes { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<QueueItem> QueueItems { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Proxy> Proxies { get; set; }
        public DbSet<ProxyLog> ProxyLogs { get; set; }
    }
}

