namespace YouQueueService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedFKSetup : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Episodes", new[] { "SeriesId" });
            DropIndex("dbo.Series", new[] { "NetworkId" });
            RenameColumn(table: "dbo.Episodes", name: "SeriesId", newName: "Series_SeriesId");
            RenameColumn(table: "dbo.Series", name: "NetworkId", newName: "Network_NetworkId");
            AlterColumn("dbo.Episodes", "Series_SeriesId", c => c.Int());
            AlterColumn("dbo.Episodes", "ExpirationDate", c => c.String());
            AlterColumn("dbo.Series", "Network_NetworkId", c => c.Int());
            CreateIndex("dbo.Episodes", "Series_SeriesId");
            CreateIndex("dbo.Series", "Network_NetworkId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Series", new[] { "Network_NetworkId" });
            DropIndex("dbo.Episodes", new[] { "Series_SeriesId" });
            AlterColumn("dbo.Series", "Network_NetworkId", c => c.Int(nullable: false));
            AlterColumn("dbo.Episodes", "ExpirationDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Episodes", "Series_SeriesId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Series", name: "Network_NetworkId", newName: "NetworkId");
            RenameColumn(table: "dbo.Episodes", name: "Series_SeriesId", newName: "SeriesId");
            CreateIndex("dbo.Series", "NetworkId");
            CreateIndex("dbo.Episodes", "SeriesId");
        }
    }
}
