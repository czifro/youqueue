namespace YouQueueService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNewFKConstraints : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Episodes", "Series_SeriesId", "dbo.Series");
            DropForeignKey("dbo.Series", "Network_NetworkId", "dbo.Networks");
            DropForeignKey("dbo.QueueItems", "Episode_EpisodeId", "dbo.Episodes");
            DropForeignKey("dbo.QueueItems", "Network_NetworkId", "dbo.Networks");
            DropForeignKey("dbo.QueueItems", "Series_SeriesId", "dbo.Series");
            DropIndex("dbo.Episodes", new[] { "Series_SeriesId" });
            DropIndex("dbo.Series", new[] { "Network_NetworkId" });
            DropIndex("dbo.QueueItems", new[] { "Episode_EpisodeId" });
            DropIndex("dbo.QueueItems", new[] { "Network_NetworkId" });
            DropIndex("dbo.QueueItems", new[] { "Series_SeriesId" });
            RenameColumn(table: "dbo.Episodes", name: "Series_SeriesId", newName: "SeriesId");
            RenameColumn(table: "dbo.Series", name: "Network_NetworkId", newName: "NetworkId");
            RenameColumn(table: "dbo.QueueItems", name: "Episode_EpisodeId", newName: "EpisodeId");
            RenameColumn(table: "dbo.QueueItems", name: "Network_NetworkId", newName: "NetworkId");
            RenameColumn(table: "dbo.QueueItems", name: "Series_SeriesId", newName: "SeriesId");
            AlterColumn("dbo.Episodes", "SeriesId", c => c.Int(nullable: false));
            AlterColumn("dbo.Series", "NetworkId", c => c.Int(nullable: false));
            AlterColumn("dbo.QueueItems", "EpisodeId", c => c.Int(nullable: false));
            AlterColumn("dbo.QueueItems", "NetworkId", c => c.Int(nullable: false));
            AlterColumn("dbo.QueueItems", "SeriesId", c => c.Int(nullable: false));
            CreateIndex("dbo.Episodes", "SeriesId");
            CreateIndex("dbo.Series", "NetworkId");
            CreateIndex("dbo.QueueItems", "NetworkId");
            CreateIndex("dbo.QueueItems", "SeriesId");
            CreateIndex("dbo.QueueItems", "EpisodeId");
            AddForeignKey("dbo.Episodes", "SeriesId", "dbo.Series", "SeriesId", cascadeDelete: true);
            AddForeignKey("dbo.Series", "NetworkId", "dbo.Networks", "NetworkId", cascadeDelete: true);
            AddForeignKey("dbo.QueueItems", "EpisodeId", "dbo.Episodes", "EpisodeId", cascadeDelete: true);
            AddForeignKey("dbo.QueueItems", "NetworkId", "dbo.Networks", "NetworkId", cascadeDelete: true);
            AddForeignKey("dbo.QueueItems", "SeriesId", "dbo.Series", "SeriesId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QueueItems", "SeriesId", "dbo.Series");
            DropForeignKey("dbo.QueueItems", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.QueueItems", "EpisodeId", "dbo.Episodes");
            DropForeignKey("dbo.Series", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.Episodes", "SeriesId", "dbo.Series");
            DropIndex("dbo.QueueItems", new[] { "EpisodeId" });
            DropIndex("dbo.QueueItems", new[] { "SeriesId" });
            DropIndex("dbo.QueueItems", new[] { "NetworkId" });
            DropIndex("dbo.Series", new[] { "NetworkId" });
            DropIndex("dbo.Episodes", new[] { "SeriesId" });
            AlterColumn("dbo.QueueItems", "SeriesId", c => c.Int());
            AlterColumn("dbo.QueueItems", "NetworkId", c => c.Int());
            AlterColumn("dbo.QueueItems", "EpisodeId", c => c.Int());
            AlterColumn("dbo.Series", "NetworkId", c => c.Int());
            AlterColumn("dbo.Episodes", "SeriesId", c => c.Int());
            RenameColumn(table: "dbo.QueueItems", name: "SeriesId", newName: "Series_SeriesId");
            RenameColumn(table: "dbo.QueueItems", name: "NetworkId", newName: "Network_NetworkId");
            RenameColumn(table: "dbo.QueueItems", name: "EpisodeId", newName: "Episode_EpisodeId");
            RenameColumn(table: "dbo.Series", name: "NetworkId", newName: "Network_NetworkId");
            RenameColumn(table: "dbo.Episodes", name: "SeriesId", newName: "Series_SeriesId");
            CreateIndex("dbo.QueueItems", "Series_SeriesId");
            CreateIndex("dbo.QueueItems", "Network_NetworkId");
            CreateIndex("dbo.QueueItems", "Episode_EpisodeId");
            CreateIndex("dbo.Series", "Network_NetworkId");
            CreateIndex("dbo.Episodes", "Series_SeriesId");
            AddForeignKey("dbo.QueueItems", "Series_SeriesId", "dbo.Series", "SeriesId");
            AddForeignKey("dbo.QueueItems", "Network_NetworkId", "dbo.Networks", "NetworkId");
            AddForeignKey("dbo.QueueItems", "Episode_EpisodeId", "dbo.Episodes", "EpisodeId");
            AddForeignKey("dbo.Series", "Network_NetworkId", "dbo.Networks", "NetworkId");
            AddForeignKey("dbo.Episodes", "Series_SeriesId", "dbo.Series", "SeriesId");
        }
    }
}
