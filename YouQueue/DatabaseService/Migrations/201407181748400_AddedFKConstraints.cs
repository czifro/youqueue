namespace YouQueueService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFKConstraints : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Episodes", "Series_SeriesId", c => c.Int());
            AddColumn("dbo.Notifications", "User_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.QueueItems", "Episode_EpisodeId", c => c.Int());
            AddColumn("dbo.QueueItems", "Network_NetworkId", c => c.Int());
            AddColumn("dbo.QueueItems", "Series_SeriesId", c => c.Int());
            AddColumn("dbo.QueueItems", "User_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.Series", "Network_NetworkId", c => c.Int());
            CreateIndex("dbo.Episodes", "Series_SeriesId");
            CreateIndex("dbo.Series", "Network_NetworkId");
            CreateIndex("dbo.Notifications", "User_Id");
            CreateIndex("dbo.QueueItems", "Episode_EpisodeId");
            CreateIndex("dbo.QueueItems", "Network_NetworkId");
            CreateIndex("dbo.QueueItems", "Series_SeriesId");
            CreateIndex("dbo.QueueItems", "User_Id");
            AddForeignKey("dbo.Series", "Network_NetworkId", "dbo.Networks", "NetworkId");
            AddForeignKey("dbo.Episodes", "Series_SeriesId", "dbo.Series", "SeriesId");
            AddForeignKey("dbo.Notifications", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.QueueItems", "Episode_EpisodeId", "dbo.Episodes", "EpisodeId");
            AddForeignKey("dbo.QueueItems", "Network_NetworkId", "dbo.Networks", "NetworkId");
            AddForeignKey("dbo.QueueItems", "Series_SeriesId", "dbo.Series", "SeriesId");
            AddForeignKey("dbo.QueueItems", "User_Id", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Episodes", "SeriesId");
            DropColumn("dbo.Notifications", "UserId");
            DropColumn("dbo.QueueItems", "UserId");
            DropColumn("dbo.QueueItems", "NetworkId");
            DropColumn("dbo.QueueItems", "SeriesId");
            DropColumn("dbo.QueueItems", "EpisodeId");
            DropColumn("dbo.Series", "NetworkId");
            DropColumn("dbo.AspNetUsers", "AccountId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "AccountId", c => c.Int(nullable: false));
            AddColumn("dbo.Series", "NetworkId", c => c.Int(nullable: false));
            AddColumn("dbo.QueueItems", "EpisodeId", c => c.Int(nullable: false));
            AddColumn("dbo.QueueItems", "SeriesId", c => c.Int(nullable: false));
            AddColumn("dbo.QueueItems", "NetworkId", c => c.Int(nullable: false));
            AddColumn("dbo.QueueItems", "UserId", c => c.Int(nullable: false));
            AddColumn("dbo.Notifications", "UserId", c => c.Int(nullable: false));
            AddColumn("dbo.Episodes", "SeriesId", c => c.Int(nullable: false));
            DropForeignKey("dbo.QueueItems", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.QueueItems", "Series_SeriesId", "dbo.Series");
            DropForeignKey("dbo.QueueItems", "Network_NetworkId", "dbo.Networks");
            DropForeignKey("dbo.QueueItems", "Episode_EpisodeId", "dbo.Episodes");
            DropForeignKey("dbo.Notifications", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Episodes", "Series_SeriesId", "dbo.Series");
            DropForeignKey("dbo.Series", "Network_NetworkId", "dbo.Networks");
            DropIndex("dbo.QueueItems", new[] { "User_Id" });
            DropIndex("dbo.QueueItems", new[] { "Series_SeriesId" });
            DropIndex("dbo.QueueItems", new[] { "Network_NetworkId" });
            DropIndex("dbo.QueueItems", new[] { "Episode_EpisodeId" });
            DropIndex("dbo.Notifications", new[] { "User_Id" });
            DropIndex("dbo.Series", new[] { "Network_NetworkId" });
            DropIndex("dbo.Episodes", new[] { "Series_SeriesId" });
            DropColumn("dbo.Series", "Network_NetworkId");
            DropColumn("dbo.QueueItems", "User_Id");
            DropColumn("dbo.QueueItems", "Series_SeriesId");
            DropColumn("dbo.QueueItems", "Network_NetworkId");
            DropColumn("dbo.QueueItems", "Episode_EpisodeId");
            DropColumn("dbo.Notifications", "User_Id");
            DropColumn("dbo.Episodes", "Series_SeriesId");
        }
    }
}
