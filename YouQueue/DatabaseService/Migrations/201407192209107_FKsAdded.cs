namespace YouQueueService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FKsAdded : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.Episodes", new[] { "Series_SeriesId" });
            DropIndex("dbo.Series", new[] { "Network_NetworkId" });
            DropIndex("dbo.QueueItems", new[] { "Episode_EpisodeId" });
            DropIndex("dbo.QueueItems", new[] { "Network_NetworkId" });
            DropIndex("dbo.QueueItems", new[] { "Series_SeriesId" });
            RenameColumn(table: "dbo.Episodes", name: "Series_SeriesId", newName: "SeriesId");
            RenameColumn(table: "dbo.Series", name: "Network_NetworkId", newName: "NetworkId");
            RenameColumn(table: "dbo.QueueItems", name: "Episode_EpisodeId", newName: "EpisodeId");
            RenameColumn(table: "dbo.QueueItems", name: "Network_NetworkId", newName: "NetworkId");
            RenameColumn(table: "dbo.QueueItems", name: "Series_SeriesId", newName: "SeriesId");
            RenameColumn(table: "dbo.QueueItems", name: "User_Id", newName: "UserId");
            RenameIndex(table: "dbo.QueueItems", name: "IX_User_Id", newName: "IX_UserId");
            AlterColumn("dbo.Episodes", "SeriesId", c => c.Int(nullable: false));
            AlterColumn("dbo.Series", "NetworkId", c => c.Int(nullable: false));
            AlterColumn("dbo.QueueItems", "EpisodeId", c => c.Int(nullable: false));
            AlterColumn("dbo.QueueItems", "NetworkId", c => c.Int(nullable: false));
            AlterColumn("dbo.QueueItems", "SeriesId", c => c.Int(nullable: false));
            CreateIndex("dbo.Episodes", "SeriesId");
            CreateIndex("dbo.Series", "NetworkId");
            CreateIndex("dbo.QueueItems", "NetworkId");
            CreateIndex("dbo.QueueItems", "SeriesId");
            CreateIndex("dbo.QueueItems", "EpisodeId");
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.QueueItems", new[] { "EpisodeId" });
            DropIndex("dbo.QueueItems", new[] { "SeriesId" });
            DropIndex("dbo.QueueItems", new[] { "NetworkId" });
            DropIndex("dbo.Series", new[] { "NetworkId" });
            DropIndex("dbo.Episodes", new[] { "SeriesId" });
            AlterColumn("dbo.QueueItems", "SeriesId", c => c.Int());
            AlterColumn("dbo.QueueItems", "NetworkId", c => c.Int());
            AlterColumn("dbo.QueueItems", "EpisodeId", c => c.Int());
            AlterColumn("dbo.Series", "NetworkId", c => c.Int());
            AlterColumn("dbo.Episodes", "SeriesId", c => c.Int());
            RenameIndex(table: "dbo.QueueItems", name: "IX_UserId", newName: "IX_User_Id");
            RenameColumn(table: "dbo.QueueItems", name: "UserId", newName: "User_Id");
            RenameColumn(table: "dbo.QueueItems", name: "SeriesId", newName: "Series_SeriesId");
            RenameColumn(table: "dbo.QueueItems", name: "NetworkId", newName: "Network_NetworkId");
            RenameColumn(table: "dbo.QueueItems", name: "EpisodeId", newName: "Episode_EpisodeId");
            RenameColumn(table: "dbo.Series", name: "NetworkId", newName: "Network_NetworkId");
            RenameColumn(table: "dbo.Episodes", name: "SeriesId", newName: "Series_SeriesId");
            CreateIndex("dbo.QueueItems", "Series_SeriesId");
            CreateIndex("dbo.QueueItems", "Network_NetworkId");
            CreateIndex("dbo.QueueItems", "Episode_EpisodeId");
            CreateIndex("dbo.Series", "Network_NetworkId");
            CreateIndex("dbo.Episodes", "Series_SeriesId");
            AddForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
