// <auto-generated />
namespace YouQueueService.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class AddedNewFKConstraints : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedNewFKConstraints));
        
        string IMigrationMetadata.Id
        {
            get { return "201407190221039_AddedNewFKConstraints"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
