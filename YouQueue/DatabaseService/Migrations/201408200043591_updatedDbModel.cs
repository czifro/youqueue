namespace YouQueueService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedDbModel : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Episodes", new[] { "Series_SeriesId" });
            DropIndex("dbo.Series", new[] { "Network_NetworkId" });
            RenameColumn(table: "dbo.Episodes", name: "Series_SeriesId", newName: "SeriesId");
            RenameColumn(table: "dbo.Series", name: "Network_NetworkId", newName: "NetworkId");
            CreateTable(
                "dbo.Genres",
                c => new
                    {
                        GenreId = c.Int(nullable: false, identity: true),
                        GenreValue = c.String(),
                    })
                .PrimaryKey(t => t.GenreId);
            
            CreateTable(
                "dbo.Proxies",
                c => new
                    {
                        ProxyId = c.Int(nullable: false, identity: true),
                        IPAddress = c.String(),
                        Port = c.Int(nullable: false),
                        UserName = c.String(),
                        Password = c.String(),
                        Domain = c.String(),
                        AnonymityLevel = c.String(),
                        AverageDelayTime = c.Double(nullable: false),
                        BlackListed = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProxyId);
            
            CreateTable(
                "dbo.ProxyLogs",
                c => new
                    {
                        ProxyLogId = c.Int(nullable: false, identity: true),
                        DelayTime = c.Int(nullable: false),
                        DidSucceed = c.Int(nullable: false),
                        ProxyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProxyLogId)
                .ForeignKey("dbo.Proxies", t => t.ProxyId)
                .Index(t => t.ProxyId);
            
            CreateTable(
                "dbo.GenreSeries",
                c => new
                    {
                        Genre_GenreId = c.Int(nullable: false),
                        Series_SeriesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Genre_GenreId, t.Series_SeriesId })
                .ForeignKey("dbo.Genres", t => t.Genre_GenreId)
                .ForeignKey("dbo.Series", t => t.Series_SeriesId)
                .Index(t => t.Genre_GenreId)
                .Index(t => t.Series_SeriesId);
            
            AlterColumn("dbo.Episodes", "SeriesId", c => c.Int(nullable: false));
            AlterColumn("dbo.Series", "NetworkId", c => c.Int(nullable: false));
            CreateIndex("dbo.Episodes", "SeriesId");
            CreateIndex("dbo.Series", "NetworkId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProxyLogs", "ProxyId", "dbo.Proxies");
            DropForeignKey("dbo.GenreSeries", "Series_SeriesId", "dbo.Series");
            DropForeignKey("dbo.GenreSeries", "Genre_GenreId", "dbo.Genres");
            DropIndex("dbo.GenreSeries", new[] { "Series_SeriesId" });
            DropIndex("dbo.GenreSeries", new[] { "Genre_GenreId" });
            DropIndex("dbo.ProxyLogs", new[] { "ProxyId" });
            DropIndex("dbo.Series", new[] { "NetworkId" });
            DropIndex("dbo.Episodes", new[] { "SeriesId" });
            AlterColumn("dbo.Series", "NetworkId", c => c.Int());
            AlterColumn("dbo.Episodes", "SeriesId", c => c.Int());
            DropTable("dbo.GenreSeries");
            DropTable("dbo.ProxyLogs");
            DropTable("dbo.Proxies");
            DropTable("dbo.Genres");
            RenameColumn(table: "dbo.Series", name: "NetworkId", newName: "Network_NetworkId");
            RenameColumn(table: "dbo.Episodes", name: "SeriesId", newName: "Series_SeriesId");
            CreateIndex("dbo.Series", "Network_NetworkId");
            CreateIndex("dbo.Episodes", "Series_SeriesId");
        }
    }
}
