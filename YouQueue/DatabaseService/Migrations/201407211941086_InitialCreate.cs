namespace YouQueueService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            AddColumn("dbo.Notifications", "User_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.QueueItems", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Episodes", "SeriesId");
            CreateIndex("dbo.Series", "NetworkId");
            CreateIndex("dbo.Notifications", "User_Id");
            CreateIndex("dbo.QueueItems", "UserId");
            CreateIndex("dbo.QueueItems", "NetworkId");
            CreateIndex("dbo.QueueItems", "SeriesId");
            CreateIndex("dbo.QueueItems", "EpisodeId");
            AddForeignKey("dbo.Series", "NetworkId", "dbo.Networks", "NetworkId");
            AddForeignKey("dbo.Episodes", "SeriesId", "dbo.Series", "SeriesId");
            AddForeignKey("dbo.Notifications", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.QueueItems", "EpisodeId", "dbo.Episodes", "EpisodeId");
            AddForeignKey("dbo.QueueItems", "NetworkId", "dbo.Networks", "NetworkId");
            AddForeignKey("dbo.QueueItems", "SeriesId", "dbo.Series", "SeriesId");
            AddForeignKey("dbo.QueueItems", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", "Id");
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Notifications", "UserId");
            DropColumn("dbo.AspNetUsers", "AccountId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "AccountId", c => c.Int(nullable: false));
            AddColumn("dbo.Notifications", "UserId", c => c.Int(nullable: false));
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.QueueItems", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.QueueItems", "SeriesId", "dbo.Series");
            DropForeignKey("dbo.QueueItems", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.QueueItems", "EpisodeId", "dbo.Episodes");
            DropForeignKey("dbo.Notifications", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Episodes", "SeriesId", "dbo.Series");
            DropForeignKey("dbo.Series", "NetworkId", "dbo.Networks");
            DropIndex("dbo.QueueItems", new[] { "EpisodeId" });
            DropIndex("dbo.QueueItems", new[] { "SeriesId" });
            DropIndex("dbo.QueueItems", new[] { "NetworkId" });
            DropIndex("dbo.QueueItems", new[] { "UserId" });
            DropIndex("dbo.Notifications", new[] { "User_Id" });
            DropIndex("dbo.Series", new[] { "NetworkId" });
            DropIndex("dbo.Episodes", new[] { "SeriesId" });
            AlterColumn("dbo.QueueItems", "UserId", c => c.Int(nullable: false));
            DropColumn("dbo.Notifications", "User_Id");
            AddForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles", "Id", cascadeDelete: true);
        }
    }
}
