using System.Collections.Generic;
using YouQueueService.Entities;

namespace YouQueueService.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<YouQueueService.YouQueueContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(YouQueueService.YouQueueContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //var proxies = new List<Proxy>(createProxyArray());
            //context.Proxies.AddOrUpdate(p => new { p.IPAddress, p.Port, p.AnonymityLevel},proxies.ToArray());
        }

        //private List<Proxy> createProxyArray()
        //{
        //    var proxyString = "23.89.198.161,8089,Elite;191.234.49.34,80,Transparent;23.89.198.161,7808,Elite;207.5.112.114,8080,Transparent;199.119.142.163,80,Elite;93.188.165.69,3128,Elite;50.22.206.179,8080,Transparent;66.228.47.169,8118,Elite;38.65.65.169,80,Elite;174.32.138.106,87,Transparent;8.19.245.223,8080,Transparent;207.190.66.99,80,Transparent;198.23.187.186,3128,Transparent;208.93.198.93,80,Transparent;107.150.12.23,80,Elite;162.243.118.227,8080,Transparent;198.46.136.179,8080,Transparent;199.21.205.102,8081,Transparent;174.136.39.121,8888,Transparent;130.211.81.40,8080,Transparent;69.57.48.251,8080,Transparent;54.88.79.127,8888,Transparent;69.255.123.97,21320,Elite;208.109.191.207,80,Elite;63.234.204.13,8080,Transparent;184.172.117.120,8081,Elite;199.21.200.13,8081,Transparent;107.170.92.239,80,Elite;107.158.13.153,3128,Elite;107.1.129.154,3128,Transparent;69.164.204.196,80,Transparent;23.226.131.35,80,Transparent;23.226.68.118,7808,Elite;54.215.17.7,9091,Elite;75.105.181.162,21320,Elite;199.119.142.163,80,Elite;173.244.181.146,33942,Anonymous;50.22.206.179,8080,Transparent;199.200.120.36,7808,Elite;93.188.165.69,3128,Elite;54.215.198.229,80,Anonymous;207.223.117.198,3128,Transparent;174.32.138.106,87,Transparent;207.190.66.99,80,Transparent;198.23.187.186,3128,Transparent;209.166.162.42,8080,Transparent;200.62.59.183,8080,Anonymous;23.251.151.31,3129,Transparent;66.228.47.169,8118,Elite;209.124.171.25,3128,Transparent;199.21.205.102,8081,Transparent;162.243.118.227,8080,Transparent;192.211.61.214,3128,Transparent;107.150.12.23,80,Elite;70.99.146.246,7004,Elite;23.238.230.199,3128,Transparent;208.109.191.207,80,Elite;198.46.136.179,8080,Transparent;23.94.44.10,3127,Elite;23.94.44.6,8089,Elite;173.245.65.54,8888,Elite;199.167.228.36,80,Elite;173.201.95.24,80,Elite;216.74.224.14,3128,Transparent;104.131.226.25,8080,Transparent;173.251.79.59,3128,Transparent;69.164.204.196,80,Transparent;54.215.17.7,9091,Elite;75.105.181.162,21320,Elite;173.244.181.146,33942,Anonymous;50.22.206.179,8080,Transparent;199.200.120.36,7808,Elite;93.188.165.69,3128,Elite;54.215.198.229,80,Anonymous;207.223.117.198,3128,Transparent;174.32.138.106,87,Transparent;207.190.66.99,80,Transparent;198.23.187.186,3128,Transparent;209.166.162.42,8080,Transparent;200.62.59.183,8080,Anonymous;23.251.151.31,3129,Transparent;66.228.47.169,8118,Elite;199.21.205.102,8081,Transparent;162.243.118.227,8080,Transparent;192.211.61.214,3128,Transparent;107.150.12.23,80,Elite;70.99.146.246,7004,Elite;208.109.191.207,80,Elite;198.46.136.179,8080,Transparent;23.94.44.10,3127,Elite;69.164.204.196,80,Transparent;54.215.17.7,9091,Elite;75.105.181.162,21320,Elite;173.244.181.146,33942,Anonymous;50.22.206.179,8080,Transparent;199.200.120.36,7808,Elite;93.188.165.69,3128,Elite;54.215.198.229,80,Anonymous;207.223.117.198,3128,Transparent;174.32.138.106,87,Transparent;207.190.66.99,80,Transparent;198.23.187.186,3128,Transparent;209.166.162.42,8080,Transparent;200.62.59.183,8080,Anonymous;23.251.151.31,3129,Transparent;66.228.47.169,8118,Elite;199.21.205.102,8081,Transparent;162.243.118.227,8080,Transparent;192.211.61.214,3128,Transparent;107.150.12.23,80,Elite;70.99.146.246,7004,Elite;208.109.191.207,80,Elite;198.46.136.179,8080,Transparent;23.94.44.10,3127,Elite;23.94.44.6,8089,Elite;54.88.135.154,8888,Transparent;68.98.216.36,3128,Transparent;74.143.72.18,3128,Transparent;199.21.200.13,8081,Transparent;50.193.66.141,7004,Elite;54.88.135.154,8888,Transparent;68.98.216.36,3128,Transparent;74.143.72.18,3128,Transparent;199.21.200.13,8081,Transparent;50.193.66.141,7004,Elite;173.245.65.158,8888,Elite;173.245.65.94,8888,Elite;8.19.245.223,8080,Transparent;69.163.47.19,8118,Elite;191.234.53.38,3128,Transparent;176.73.127.191,3128,Elite;108.60.201.176,3128,Elite;64.19.28.6,8080,Transparent;107.170.198.142,80,Elite;199.21.200.80,8081,Transparent;54.88.50.140,80,Anonymous;107.178.209.197,80,Elite;173.245.65.38,8888,Elite;107.155.187.154,7808,Elite;95.215.46.63,3128,Elite;50.201.112.10,80,Transparent;162.220.218.173,7808,Elite;173.245.65.150,8888,Elite;69.74.231.173,8080,Transparent;108.61.49.178,7808,Elite;207.87.237.220,3128,Transparent;192.3.110.146,8080,Transparent;204.27.58.202,3128,Transparent;23.92.222.117,7808,Elite;65.48.113.25,9999,Transparent;50.201.112.10,80,Transparent;162.220.218.173,7808,Elite;173.245.65.150,8888,Elite;69.74.231.173,8080,Transparent;108.61.49.178,7808,Elite;207.87.237.220,3128,Transparent;192.3.110.146,8080,Transparent;204.27.58.202,3128,Transparent;65.48.113.25,9999,Transparent;66.85.131.18,7808,Elite;206.165.4.40,8888,Elite;206.165.4.34,8888,Elite;198.2.202.33,80,Anonymous;23.89.198.161,7808,Elite;208.93.198.93,80,Transparent;38.108.117.124,3128,Transparent;107.170.85.127,8118,Elite;23.251.155.224,80,Elite;199.21.200.2,8081,Transparent;54.91.92.141,80,Anonymous;107.18.35.253,8080,Transparent;162.220.9.139,8080,Transparent;207.5.112.114,8080,Transparent;173.245.65.30,8888,Elite;209.62.12.130,8118,Elite;173.245.65.86,8888,Elite;69.57.48.251,8080,Transparent;173.245.65.6,8888,Elite;173.245.65.134,8888,Elite;54.88.79.127,8888,Transparent";

        //    var proxyList = proxyString.Split(';');

        //    var proxies = proxyList.Select(s => s.Split(',')).Select(temp => new Proxy
        //    {
        //        IPAddress = temp[0], Port = Convert.ToInt32(temp[1]), AnonymityLevel = temp[2]
        //    }).ToList();

        //    return proxies;
        //}
    }
}
