﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;

namespace YouQueueService.Entities
{
    public class Network
    {
        public int NetworkId { get; set; }
        [Display(Name = "Network Name")]
        public string NetworkName { get; set; }
        public string Url { get; set; }
        public string Thumbnail { get; set; }
        [JsonIgnore]
        public int HuluNetworkId { get; set; }
        [JsonIgnore]
        public virtual ICollection<Series> Series { get; set; }
    }
}
