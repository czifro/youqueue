﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
namespace YouQueueService.Entities
{
    public class Notification
    {
        public int NotificationId { get; set; }
        [Display(Name = "Message")]
        public string Message { get; set; }
        [Display(Name = "Timestamp")]
        public DateTime Timestamp { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
