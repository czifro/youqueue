﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;

namespace YouQueueService.Entities
{
    public class Episode
    {
        public int EpisodeId { get; set; }
        [Display(Name = "Episode Name")]
        public string EpisodeName { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Episode Number")]
        public int EpisodeNum { get; set; }
        [Display(Name = "Season Number")]
        public int SeasonNum { get; set; }
        public string Thumbnail { get; set; }
        [Display(Name = "Expiration Date")]
        public string ExpirationDate { get; set; }
        [JsonIgnore]
        public int SeriesId { get; set; }
        [JsonIgnore]
        public virtual Series Series { get; set; }
    }
}
