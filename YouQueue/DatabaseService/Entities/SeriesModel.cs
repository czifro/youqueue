﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;

namespace YouQueueService.Entities
{
    public class Series
    {
        public int SeriesId { get; set; }
        [Display(Name = "Series Name")]
        public string SeriesName { get; set; }
        public string SeriesUrl { get; set; }
        [Display(Name = "Genre")]
        public string Genre { get; set; }
        [Display(Name = "Year")]
        public string Year { get; set; }
        [Display(Name = "Update Day")]
        public string UpdateDay { get; set; }
        public string Thumbnail { get; set; }
        [JsonIgnore]
        public int HuluShowId { get; set; }
        [JsonIgnore]
        public int NetworkId { get; set; }
        [JsonIgnore]
        public virtual Network Network { get; set; }
        [JsonIgnore]
        public virtual ICollection<Episode> Episodes { get; set; }
        [JsonIgnore]
        public virtual ICollection<Genre> Genres { get; set; }
    }

    public class Genre
    {
        public int GenreId { get; set; }
        public string GenreValue { get; set; }
        [JsonIgnore]
        public virtual ICollection<Series> Series { get; set; }
    }
}
