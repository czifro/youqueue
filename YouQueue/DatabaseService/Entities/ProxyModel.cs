﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace YouQueueService.Entities
{
    public class Proxy
    {
        public int ProxyId { get; set; }
        public string IPAddress { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public string AnonymityLevel { get; set; }
        public double AverageDelayTime { get; set; }
        public int BlackListed { get; set; }
        [NotMapped]
        public bool IsUsable { get; set; }
        [NotMapped]
        public double TempValue { get; set; }
        public virtual ICollection<ProxyLog> ProxyLogs { get; set; }
    }

    public class ProxyLog
    {
        public int ProxyLogId { get; set; }
        public int DelayTime { get; set; }
        public int DidSucceed { get; set; }
        public int ProxyId { get; set; }
        [JsonIgnore]
        public virtual Proxy Proxy { get; set; }
    }
}
