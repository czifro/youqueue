﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;


namespace YouQueueService.Entities
{
    public class QueueItem
    {
        public int QueueItemId { get; set; }
        public string UserId { get; set; }
        
        public int NetworkId { get; set; }
        public int SeriesId { get; set; }
        public int EpisodeId { get; set; }
        /// <summary>
        /// 0 keep, 1 remove
        /// </summary>
        [Display(Name = "Delete Item")]
        public int DeleteItem { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual Network Network { get; set; }
        public virtual Series Series { get; set; }
        public virtual Episode Episode { get; set; }
    }
}
