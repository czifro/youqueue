﻿using System;
using YouQueueService.Entities;
using YouQueueService.Helpers;
using YouQueueService.NetworkServices;


namespace YouQueueTest
{
    class Console
    {
        public static void Main(string[] args)
        {
            var console = new Console();
            console.RunProxy();
        }

        public void RunProxy()
        {
            var pr = new Proxy { IPAddress = "23.238.230.199", Port = 3128 };
            HuluClient.SetWebProxy(ProxyHelper.ToWebProxy(pr));
            string json;
            TimeSpan timeSpan;
            HuluClient.GetHuluAccessToken(true);
            pr = new Proxy { IPAddress = "54.215.198.229", Port = 80 };
            HuluClient.SetWebProxy(ProxyHelper.ToWebProxy(pr));
            HuluClient.MakeCallToHuluApi(2, out json, out timeSpan);
            ProxyHelper.GenerateAndInsertProxyLog(pr, timeSpan, true);
        }
    }
}
