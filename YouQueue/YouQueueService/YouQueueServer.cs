﻿using System;
using System.Linq;
using YouQueueService;
using YouQueueService.Entities;
using YouQueueService.Manager;
using YouQueueService.NetworkServices;
using YouQueueService.NetworkServices;
using YouQueueService.ParsingServices.ParseHtml;
using YouQueueService.ParsingServices.ParseJson;

namespace YouQueueServer
{
    class YouQueueServer
    {
        private YouQueueContext _context;
        private DatabaseManager _manager;
        private readonly HtmlParser _htmlParser;
        private readonly HuluParser _huluParser;
        private readonly WebPageDownloader _webPageDownloader;

        public YouQueueServer()
        {
            _webPageDownloader = new WebPageDownloader();
            _htmlParser = new HtmlParser();
            _huluParser = new HuluParser();
        }

        public void Run()
        {
            //while (true)
            //{
            //    try
            //    {
            //        _context = new YouQueueContext();
            //        _manager = new DatabaseManager(ref _context);
            //        var startTime = DateTime.Now;
            //        UpdateNetworks();
            //        UpdateSeries();
            //        UpdateEpisodes();
            //        var endTime = DateTime.Now;
            //        var timeLapse = endTime - startTime;
            //        var t = timeLapse;
            //        _manager.SaveAndDispose();
            //    }
            //    catch (Exception)
            //    {
            //        Trace.TraceInformation("Failed to run process");
            //    }

            //    Thread.Sleep(new TimeSpan(0, 0, 0, 5));
            //}
        }



        //private void UpdateNetworks()
        //{
        //    try
        //    {
        //        UpdateAccessToken();
        //        Constants.HuluParamSort = "popular_this_month";
        //        var json = HuluClient.GetHuluJsonData(Constants.ConstructWhiteListHuluNetworksUrl());
        //        _huluParser.Parse(json, 3);
        //        _manager.UploadNetworks(_huluParser.NetworkList);
        //        _huluParser.NetworkList = new List<Network>();
        //    }
        //    catch (Exception)
        //    {
        //        Trace.TraceError("Failed To Upload Networks");
        //        throw new Exception("Upload Failed");
        //    }
        //}

        //private void UpdateSeries()
        //{
        //    try
        //    {
        //        var networks = _context.Networks.Select(n => n).ToList();
        //        networks.AddRange(_manager.BufferedNetworks);
        //        foreach (var network in networks)
        //        {
        //            UpdateAccessToken();
        //            Constants.HuluParamSort = "popular_today";
        //            Constants.HuluNetworkId = network.HuluNetworkId.ToString();
        //            var json = HuluClient.GetHuluJsonData(Constants.ConstructHuluSeriesUrl());
        //            _huluParser.CurrentNetwork = network;
        //            _huluParser.Parse(json, 2);
        //            _manager.UploadSeries(_huluParser.SeriesList);
        //            _huluParser.SeriesList = new List<Series>();
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        Trace.TraceError("Failed To Upload Series");
        //        throw new Exception("Upload Failed");
        //    }
        //}

        //private void UpdateEpisodes()
        //{
        //    try
        //    {
        //        var series = _context.Series.Select(s => s).ToList();
        //        series.AddRange(_manager.BufferedSeries);
        //        foreach (var s in series)
        //        {
        //            UpdateAccessToken();
        //            Constants.HuluParamSort = "seasons_and_release";
        //            Constants.HuluShowId = s.HuluShowId.ToString();
        //            var json = HuluClient.GetHuluJsonData(Constants.ConstructHuluEpisodesPrimaryUrl());
        //            _huluParser.CurrentSeries = s;
        //            if (!_huluParser.Parse(json, 1))
        //            {
        //                json = HuluClient.GetHuluJsonData(Constants.ConstructHuluEpisodesSecondaryUrl());
        //            }
        //            _huluParser.Parse(json, 1);
        //            if (!_huluParser.EpisodesList.Any())
        //            {
        //                _manager.BufferedSeries.Remove(s);
        //                continue;
        //            }
        //            _manager.UploadEpisodes(_huluParser.EpisodesList);
        //            _huluParser.EpisodesList = new List<Episode>();
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        Trace.TraceError("Failed to upload Episodes");
        //        throw new Exception("Upload Failed");
        //    }
        //}

        private void UpdateAccessToken()
        {
            try
            {
                var html = _webPageDownloader.GetHtmlAsString(HuluClient.HuluBaseUrl);
                _htmlParser.GetHuluAccessToken(html);
            }
            catch (Exception)
            {
                throw new Exception("Failed to get Access Token");
            }
        }
    }
}
