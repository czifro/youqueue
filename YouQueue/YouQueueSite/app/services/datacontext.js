(function () {
    'use strict';

    var serviceId = 'datacontext';
    angular.module('app').factory(serviceId, ['common', 'config', '$http', '$location', datacontext]);

    function datacontext(common, config, $http, $location) {
        var $q = common.$q;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(serviceId);
        var logError = getLogFn(serviceId, 'error');

        var service = {
            getNetworks: getNetworks,
            getSeries: getSeries,
            getEpisodes: getEpisodes
        };

        return service;

        function getNetworks() {
            var innerconfig = {
                url: "/api/v1/data/getNetworks",
                method: "GET",
                headers: {
                    'Accept': 'text/json'
                }
            };

            return $http(innerconfig).then(onSuccess, requestFailed);

            function onSuccess(results) {
                if (results && results.data) {
                    return results.data;
                }
                return null;
            }
        }

        function getSeries(networkId) {
            var innerconfig = {
                url: "/api/v1/data/getSeries",
                params: { networkId: networkId },
                method: "GET",
                headers: {
                    'Accept': 'text/json'
                }
            };

            return $http(innerconfig).then(onSuccess, requestFailed);

            function onSuccess(results) {
                if (results && results.data) {
                    return results.data;
                }
                return null;
            }
        }

        function getEpisodes(seriesId) {
            var innerconfig = {
                url: "/api/v1/data/getEpisodes",
                params: { seriesId: seriesId },
                method: "GET",
                headers: {
                    'Accept': 'text/json'
                }
            };

            return $http(innerconfig).then(onSuccess, requestFailed);

            function onSuccess(results) {
                if (results && results.data) {
                    return results.data;
                }
                return null;
            }
        }

        function requestFailed(error) {
            if (error.status == 401) {
                logError('Error: Unauthorized, please login again.', 'ERROR', true);
                //$location.path('/login');

            }
            else if (error.status == 404) {
                logError('Could not access data for your selection');
            }
            else {
                var msg = "Error";
                if (error.message)
                    msg = msg + ": " + error.message;

                if (error.data.Message)
                    msg = msg + ": " + error.data.Message;

                logError(msg, 'ERROR', true);
            }

            return $q.reject(error);
        }
    }
})();