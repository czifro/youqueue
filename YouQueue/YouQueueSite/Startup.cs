﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using YouQueueService;

namespace YouQueueSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Constants.LoadConstants(new YouQueueContext());
        }
    }
}