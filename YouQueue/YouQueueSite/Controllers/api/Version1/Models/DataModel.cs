﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YouQueueSite.Controllers.Api.Version1.Models
{
    public class NetworkModel
    {
        [Required]
        [Display(Name = "NetworkId")]
        public int NetworkId { get; set; }

        [Required]
        [Display(Name = "NetworkName")]
        public string NetworkName { get; set; }
        
        [Display(Name = "Url")]
        public string Url { get; set; }

        [Display(Name = "ThumbUrl")]
        public string ThumbUrl { get; set; }
    }

    public class SeriesModel
    {
        [Required]
        [Display(Name = "SeriesId")]
        public int SeriesId { get; set; }
        [Required]
        [Display(Name = "SeriesName")]
        public string SeriesName { get; set; }
    }

    public class FilterModel
    {
        [Display(Name = "Genre")]
        public string Genre { get; set; }
        [Display(Name = "Year")]
        public string Year { get; set; }

    }
}