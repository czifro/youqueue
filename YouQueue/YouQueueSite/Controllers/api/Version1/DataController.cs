﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using YouQueueService;
using YouQueueSite.Attributes;
using YouQueueSite.Controllers.Api.Version1.Models;

namespace YouQueueSite.Controllers.Api.Version1
{
    public class DataController : ApiController
    {
        private readonly YouQueueContext _context;

        public DataController(YouQueueContext context)
        {

            _context = context;
        }

        public HttpResponseMessage GetNetworks()
        {
            var networks = _context.Networks.Select(n => n);

            if (!networks.Any())
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));

            return Request.CreateResponse(HttpStatusCode.OK, networks);
        }

        [HttpGet]
        public HttpResponseMessage GetSeries(int networkId)
        {
            //var series = from s in _context.Series
            //    where s.NetworkId == networkId
            //    select new
            //    {
            //        s.Genre,
            //        s.SeriesName,
            //        s.SeriesId,
            //        s.Thumbnail
            //    };

            //if (!series.Any())
            //    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));

            return Request.CreateResponse(HttpStatusCode.OK);//, series);
        }

        [HttpGet]
        public HttpResponseMessage GetSeriesByGenre(string genre)
        {
            var series = _context.Series.Where(s => s.Genre.Contains(genre));
            
            if (!series.Any())
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));

            return Request.CreateResponse(HttpStatusCode.OK, series);
        }

        [HttpGet]
        public HttpResponseMessage GetEpisodes(int seriesId)
        {
            //var episodes = from e in _context.Episodes
            //    where e.SeriesId == seriesId
            //    select new
            //    {
            //        e.EpisodeId,
            //        e.EpisodeName,
            //        e.EpisodeNum,
            //        e.ExpirationDate,
            //        e.Description,
            //        e.Thumbnail
            //    };

            //if (!episodes.Any())
            //    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));

            return Request.CreateResponse(HttpStatusCode.OK);//, episodes);
        }
    }
}
