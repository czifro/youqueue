﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Web.Security;

namespace YouQueueSite.Attributes
{
	/// <summary>
    /// usage [HeaderAuthentication]
	/// </summary>
	public class HeaderAuthenticationAttribute : ActionFilterAttribute
	{       
		public override void OnActionExecuting(HttpActionContext actionContext)
		{
            IEnumerable<string> apiKeyHeaderValues;
		    if (!actionContext.Request.Headers.TryGetValues("X-User-Token", out apiKeyHeaderValues))
		        throw new HttpResponseException(actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
		            "Invalid token."));

		    var tokenstr = apiKeyHeaderValues.First();

            //User user;
		    try
		    {
                //var userTicket = FormsAuthentication.Decrypt(tokenstr);
                //var context = new BrowsarEntities();
                //user = context.Users.FirstOrDefault(u => u.Email == userTicket.Name && !userTicket.Expired);
		    }
		    catch (CryptographicException)
		    {
		        throw new HttpResponseException(actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
		            "Invalid token."));
		    }
		    catch (FormatException)
		    {
                throw new HttpResponseException(actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
                    "Invalid token."));
		    }

            //UserNullCheck(actionContext, user);

            //var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, user.Email) }, "User");
            //var principal = new ClaimsPrincipal(identity);

            //Thread.CurrentPrincipal = principal;
            //HttpContext.Current.User = principal;
		}

        //[DebuggerStepThrough]
        //private static void UserNullCheck(HttpActionContext actionContext, User user)
        //{
        //    if (user == null)
        //        throw new HttpResponseException(actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
        //            "Invalid token."));
        //}
	}

}