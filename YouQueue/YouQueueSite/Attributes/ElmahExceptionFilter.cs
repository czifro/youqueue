﻿using System.Web.Http.Filters;
using Elmah;

namespace YouQueueSite.Attributes
{
    public class ElmahExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            base.OnException(context);
            ErrorSignal.FromCurrentContext().Raise(context.Exception);
        }
    }
}