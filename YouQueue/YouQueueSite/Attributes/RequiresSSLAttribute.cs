﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;

namespace YouQueueSite.Attributes
{
    public class RequiresSSLAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase req = filterContext.HttpContext.Request;
            HttpResponseBase res = filterContext.HttpContext.Response;

            //Check if we're secure or not and if we're on the local box
            if (!req.IsSecureConnection && !req.IsLocal && ConfigurationManager.AppSettings["SSL"]=="true")
            {
                var builder = new UriBuilder(req.Url)
                    {
                        Scheme = Uri.UriSchemeHttps,
                        Port = 443
                    };
                res.Redirect(builder.Uri.ToString());
            }
            base.OnActionExecuting(filterContext);
        }
    }
}