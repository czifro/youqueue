﻿using System.Web;
using System.Web.Mvc;

namespace YouQueueSite.Attributes
{

	public class NewSessionRedirectAttribute : ActionFilterAttribute
	{
		public string Url { get; set; }
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			// check if a new session id was generated
			if (HttpContext.Current.Session.IsNewSession) HttpContext.Current.Response.Redirect(Url);

			base.OnActionExecuting(filterContext);
		}
	}
}