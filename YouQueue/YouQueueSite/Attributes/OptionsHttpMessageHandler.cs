﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace YouQueueSite.Attributes
{
    public class OptionsHttpMessageHandler : DelegatingHandler
    {
        //http://blogs.msdn.com/b/carlosfigueira/archive/2012/02/20/implementing-cors-support-in-asp-net-web-apis.aspx
        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Method == HttpMethod.Options)
            {
                var apiExplorer = GlobalConfiguration.Configuration.Services.GetApiExplorer();

                var controllerRequested = request.GetRouteData().Values["controller"] as string;
                var supportedMethods = apiExplorer.ApiDescriptions
                                                  .Where(d =>
                                                      {
                                                          var controller = d.ActionDescriptor.ControllerDescriptor.ControllerName;
                                                          return string.Equals(
                                                              controller, controllerRequested, StringComparison.OrdinalIgnoreCase);
                                                      })
                                                  .Select(d => d.HttpMethod.Method)
                                                  .Distinct();

                if (!supportedMethods.Any())
                    return Task.Factory.StartNew(
                        () => request.CreateResponse(HttpStatusCode.NotFound));

                return Task.Factory.StartNew(() =>
                    {
                        var originheader = request.Headers.FirstOrDefault(h => h.Key.ToLower() == "origin");

                        HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK);

                        response.Headers.Add("Access-Control-Allow-Origin", (originheader.Value != null && originheader.Value.Any() ? originheader.Value.FirstOrDefault() : "*"));
                        response.Headers.Add("Access-Control-Allow-Credentials", "true");
                        response.Headers.Add("Access-Control-Allow-Methods", string.Join(",", supportedMethods));
                        response.Headers.Add("Access-Control-Allow-Headers", "Content-Type,X-User-Token,X-Name");

                        return response;
                    });
            }

            return base.SendAsync(request, cancellationToken);
        }
    }
}