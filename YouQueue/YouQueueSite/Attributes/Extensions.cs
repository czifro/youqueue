﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.WebHost;
using System.Web.Routing;
using System.Web.SessionState;

namespace YouQueueSite.Attributes
{
    public static class Extensions
    {
//        public class SessionControllerHandler : HttpControllerHandler, IRequiresSessionState
//        {
//            public SessionControllerHandler(RouteData routeData)
//                : base(routeData)
//            { }
//        }

//        public class SessionRouteHandler : IRouteHandler
//        {
//            IHttpHandler IRouteHandler.GetHttpHandler(RequestContext requestContext)
//            {
//                return new SessionControllerHandler(requestContext.RouteData);
//            }
//        }

//        public static string PrependHost(this string path, string hostname)
//        {
//            return (path.Substring(0, 4) != "http" ? "http://" + hostname + (path.Substring(0, 1) != "/" ? "/" : "") : "") + path;
//        }


        public static HttpResponseMessage CreateDomainResponse(this HttpRequestMessage request, HttpStatusCode statusCode)
        {
            var originheader = request.Headers.FirstOrDefault(h => h.Key.ToLower() == "origin");

            HttpResponseMessage response = request.CreateResponse(statusCode);

            response.Headers.Add("Access-Control-Allow-Origin", (originheader.Value != null && originheader.Value.Any() ? originheader.Value.FirstOrDefault() : "*"));
            response.Headers.Add("Access-Control-Allow-Credentials", "true");
            response.Headers.Add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
            response.Headers.Add("Access-Control-Allow-Headers", "Content-Type,X-User-Token,X-Name");

            return response;
        }

//        public static HttpResponseMessage CreateDomainResponse<T>(this HttpRequestMessage request, HttpStatusCode statusCode, T value)
//        {
//            var originheader = request.Headers.FirstOrDefault(h => h.Key.ToLower() == "origin");
//
//            HttpResponseMessage response = request.CreateResponse(statusCode, value);
//
//            response.Headers.Add("Access-Control-Allow-Origin", (originheader.Value != null && originheader.Value.Any() ? originheader.Value.FirstOrDefault() : "*"));
//            response.Headers.Add("Access-Control-Allow-Credentials", "true");
//            response.Headers.Add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
//            response.Headers.Add("Access-Control-Allow-Headers", "Content-Type,X-User-Token");
//
//            return response;
//        }
    }
}