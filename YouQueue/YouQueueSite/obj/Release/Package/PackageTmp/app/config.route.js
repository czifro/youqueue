﻿(function () {
    'use strict';

    var app = angular.module('app');

    // Collect the routes
    app.constant('routes', getRoutes());
    
    // Configure the routes and route resolvers
    app.config(['$routeProvider', 'routes', routeConfigurator]);
    function routeConfigurator($routeProvider, routes) {

        routes.forEach(function (r) {
            $routeProvider.when(r.url, r.config);
        });
        $routeProvider.otherwise({ redirectTo: '/browse' });
    }

    // Define the routes 
    function getRoutes() {
        return [
            {
                url: '/browse',
                config: {
                    templateUrl: 'app/welcome/welcome.html',
                    title: 'browse',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-search"></i> Browse'
                    }
                }
            }, {
                url: '/my-queue',
                config: {
                    templateUrl: 'app/myQueue/myQueue.html',
                    title: 'my-queue',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-list"></i> My Queue'
                    }
                }
            }, {
                url: '/admin',
                config: {
                    title: 'admin',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 3,
                        content: '<i class="fa fa-lock"></i> Admin'
                    }
                }
            }, {
                url: '/api',
                config: {
                    title: 'api',
                    templateUrl: 'app/uqApi/uqApi.html',
                    settings: {
                        nav: 4,
                        content: '<i class="fa fa-code"></i> U.Q. API'
                    }
                }
            }
        ];
    }
})();