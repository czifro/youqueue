﻿(function() {
    'use strict';
    var controllerId = 'uqApi';

    angular.module('app').controller(controllerId, ['common', 'datacontext', '$rootScope', uqApi]);

    function uqApi(common, datacontext, $rootScope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;

        activate();

        function activate() {
            var promises = [];
            common.activateController(promises, controllerId)
                .then(function () {
                    return log('Activated U.Q API View');
                });
        }
    };
})();