﻿(function () {
    'use strict';
    var controllerId = 'welcome';
    angular.module('app').controller(controllerId, ['common', 'datacontext', '$rootScope', welcome]);

    function welcome(common, datacontext, $rootScope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.showNetworks = true;
        vm.networks = [];
        vm.selectedNetwork = null;
        vm.showSeries = false;
        vm.series = [];
        vm.selectedSeries = null;
        vm.showEpisodes = false;
        vm.episodes = [];

        activate();

        function activate() {
            var promises = [getNetworks()];
            common.activateController(promises, controllerId)
                .then(function () {
                    return log('Activated Welcome View');
                });
        }

        function getNetworks() {
            return datacontext.getNetworks().then(function(data) {
                return vm.networks = data;
            });
        }

        vm.selectNetwork = function (network) {
            vm.selectedNetwork = network;
            return datacontext.getSeries(network.NetworkId).then(function (data) {
                vm.showNetworks = false;
                vm.showSeries = true;
                return vm.series = data;
            });
        }

        vm.selectSeries = function (series) {
            vm.selectedSeries = series;
            return datacontext.getEpisodes(series.SeriesId).then(function(data) {
                //todo: show modal
                vm.showEpisodes = true;
                vm.showSeries = false;
                vm.episodes = data;
                return;
            });
        }

        vm.getEpiName = function (episode) {
            episode.epiName = episode.EpisodeName.length > 10 ? episode.EpisodeName.substring(0, 10) + '...' : episode.EpisodeName;
            return false;
        }

        vm.selectedEpisode = function(episode) {
            $rootScope.setEpisode(episode);
            $('#modal-episode-info').modal('show');
        }

        vm.goBack = function(level) {
            switch(level) {
                case 1:
                    vm.showEpisodes = false;
                    vm.showSeries = false;
                    vm.showNetworks = true;
                    vm.selectedSeries = null;
                    vm.selectedNetwork = null;
                    break;
                case 2:
                    vm.showEpisodes = false;
                    vm.showSeries = true;
                    vm.selectedSeries = null;
                    break;
            }
        }

        

    }
})();