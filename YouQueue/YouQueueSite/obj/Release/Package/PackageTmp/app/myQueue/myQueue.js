﻿(function () {
    'use strict';
    var controllerId = 'myQueue';
    angular.module('app').controller(controllerId, ['common', 'datacontext', myQueue]);

    function myQueue(common, datacontext) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        var vm = this;
        vm.news = {
            title: 'My Queue',
            description: 'No items are in your queue yet'
        };
        vm.title = 'My Queue';

        activate();

        function activate() {
            var promises = [];
            common.activateController(promises, controllerId)
                .then(function () { log('Activated My Queue View'); });
        }
    }
})();