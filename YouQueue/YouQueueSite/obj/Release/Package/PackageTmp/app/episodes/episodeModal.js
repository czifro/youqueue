﻿(function() {
    'use strict';
    var controllerId = 'episodeModal';

    angular.module('app').controller(controllerId, ['common', 'datacontext', '$rootScope', episodeId]);

    function episodeId(common, datacontext, $rootScope) {
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var logError = getLogFn(controllerId, 'error');

        var vm = this;
        vm.episode = null;
        $rootScope.setEpisode = function(episode) {
            vm.episode = episode;
        }
        

    }
})();