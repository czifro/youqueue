﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using Newtonsoft.Json;
using YouQueueSite.Attributes;
using YouQueueSite.Formatters;

namespace YouQueueSite.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("AccountsApi", "api/v{version}/accounts/{action}/{id}",
                new { controller = "Accounts", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute("DataApi", "api/v{version}/data/{action}/{id}",
                new { controller = "Data", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute("NotificationApi", "api/v{version}/notifications/{action}/{id}",
                new { controller = "Notifications", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute("QueueApi", "api/v{version}/queue/{action}/{id}",
                new { controller = "Queue", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute("DefaultApi", "api/v{version}/{controller}/{id}",
                new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/plain"));
            //config.Formatters.Remove(config.Formatters.FormUrlEncodedFormatter);
            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Filters.Add(new ElmahExceptionFilter()); // register elmah for web api
            // =sg= config.Filters.Add(new ExceptionHandlingAttribute());

            // Uncommenting these will apply         [ValidateModelState] and [CheckModelForNull] to all Web API Methods
            GlobalConfiguration.Configuration.Filters.Add(new CheckModelForNullAttribute());
            GlobalConfiguration.Configuration.Filters.Add(new ValidateModelStateAttribute());

            GlobalConfiguration.Configuration.Formatters.Add(new MultiPartFormatter());

            // These will set the JSON.Net serializer settings for WebApi Methods returning an object
            config.Formatters.JsonFormatter.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
        }
    }
}
